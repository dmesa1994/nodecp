const fs = require('fs-extra');
const fsNative = require('fs');
const express = require('express')
const app = express()
const port = 3000

const originPath = "/tmp/sql.tmp";
const destinationPath = "/mnt/azure/sql.tmp.copied";

const reverseOriginPath = "/mnt/azure/sql.vol"
const reverseDestinationPath = "/tmp/sql.vol.copied"

function copyFile(source, destination) {
  const readStream = fsNative.createReadStream(source);
  const writeStream = fsNative.createWriteStream(destination);

  readStream.on('error', (err) => {
    console.error('Error reading the source file:', err);
  });

  writeStream.on('error', (err) => {
    console.error('Error writing to the destination file:', err);
  });

  readStream.pipe(writeStream);

  readStream.on('end', () => {
    console.log('File copied successfully!');
  });
}

app.get('/hello', (req, res) => {
  const user = req.query.user || 'user'
  res.send(`hello ${user}!`);
})

app.get('/', (req, res) => {
  try {
    // copyFile(originPath, destinationPath);
    fsNative.copyFileSync(originPath, destinationPath, fsNative.constants.COPYFILE_EXCL);
    // fs.writeFileSync(originPath, fs.readFileSync(destinationPath));
  } catch (error) {
    console.log(error);
    res.send('Error: ' + error.message);
  }
  
  try {
    // copyFile(reverseOriginPath, reverseDestinationPath);
    fsNative.copyFileSync(reverseOriginPath, reverseDestinationPath, fsNative.constants.COPYFILE_EXCL);
    // fs.writeFileSync(reverseOriginPath, fs.readFileSync(reverseDestinationPath));
  } catch (error) {
    console.log(error);
    res.send('Error: ' + error.message);
  }
  res.send('copied!');
});

app.get('/copy', (req, res) => {
  try {
    fsNative.copyFileSync(req.query.origin, req.query.destination, fsNative.constants.COPYFILE_EXCL);
    res.send('copied!');
  } catch (error) {
    console.log('Error: ' + error);
  }
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})


/*fs.copy(originPath, destinationPath, { overwrite: true })
    .then(() => console.log('success!'))
    .catch(err => console.error(err))*/

//export PATH=$PATH:/layers/heroku_nodejs-engine/dist/bin/:/layers/heroku_nodejs-npm/npm/bin/
