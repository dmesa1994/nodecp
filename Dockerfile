FROM node:16-alpine

RUN apk add --update nano curl

WORKDIR /app

COPY package.json ./

RUN npm install && npm install express

COPY . .

USER node

CMD [ "node", "/app/index.js" ]
